# cheap_LFBRMR

Adapt very classic schematic to a very cheap robot. 

Keep it cheap using only spare parts from my current own stock, and uses no microcontroler.

# Robot type 

This one is very very simple, it lights down to the floor, then light sensor is connected as a comparator and can trigger side motor.

this is symmetric so it can go a little on left or on right to correct trajectory.

This method is limited to black track on lighter colors on the ground. Track must have smooth curves too.

It is enough to play on floor with dark tape _to be easy to remove and clean_.

# kicad 

![schema](./4.png)

![pcb b](./1.png)

![pcb a](./2.png)

design ideas : 

![can look like](./3.png)

![can olso be like](./5.png)

